﻿using System.Collections.Generic;

namespace GildedRose
{
	class GildedRose
	{
	    readonly IList<Item> _items;

		public GildedRose(IList<Item> items) 
		{
            _items = items;
		}
		
		public void UpdateQuality()
		{
		    foreach (var item in _items)
		    {
		        UpdateItemQuality(item);
		    }
		}

	    private void UpdateItemQuality(Item item)
        {
            if (item.IsLegendary())
            {
                return;
            }

            item.SellIn = item.SellIn - 1;
            
            if (item.IsAgedBrie())
	        {
	            item.IncreaseQuality();
	        }
            else if (item.IsConcert())
            {
                item.IncreaseQuality();

                if (item.SellIn < 10)
                {
                    item.IncreaseQuality();
                }

                if (item.SellIn < 5)
                {
                    item.IncreaseQuality();
                }
            }
            else
	        {
	            item.DecreaseQuality();
	            if (item.IsConjured())
	            {
	                item.DecreaseQuality();
	            }
	        }
            
	        if (item.IsExpired())
	        {
	            if (item.IsAgedBrie())
	            {
	                item.IncreaseQuality();
	            }
	            else
	            {
	                if (item.IsConcert())
	                    item.Quality = 0;
	                else
	                    item.DecreaseQuality();
	            }
	        }
	    }
	}

    public static class ItemExtensions
    {
        public static bool IsExpired(this Item item)
        {
            return item.SellIn < 0;
        }

        public static bool IsConcert(this Item item)
        {
            return item.Name == "Backstage passes to a TAFKAL80ETC concert";
        }

        public static bool IsAgedBrie(this Item item)
        {
            return item.Name == "Aged Brie";
        }

        public static void DecreaseQuality(this Item item)
        {
            if (item.Quality > 0)
            {
                item.Quality = item.Quality - 1;
            }
        }

        public static void IncreaseQuality(this Item item)
        {
            if (item.Quality < 50)
            {
                item.Quality = item.Quality + 1;
            }
        }

        public static bool IsLegendary(this Item item)
        {
            return item.Name == "Sulfuras, Hand of Ragnaros";
        }

        public static bool IsConjured(this Item item)
        {
            return item.Name.Contains("Conjured");
        }
    }

	public class Item
	{
		public string Name { get; set; }
		
		public int SellIn { get; set; }
		
		public int Quality { get; set; }
	}
	
}
