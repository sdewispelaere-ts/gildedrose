﻿using System;
using NUnit.Framework;
using System.Collections.Generic;

namespace GildedRose
{
    [TestFixture()]
    public class GildedRoseTest
    {
        private IList<Item> _items;
        private GildedRose _gildedRose;

        [SetUp]
        public void Setup()
        {
            _items = new List<Item>();
            _gildedRose = new GildedRose(_items);

        }


        [Test]
        public void SellInDecreaseAndQualityDecrease()
        {
            var item = new Item { Name = "foo", SellIn = 10, Quality = 10 };
            _items.Add(item);
            _gildedRose.UpdateQuality();
            Assert.AreEqual("foo", item.Name);
            Assert.AreEqual(9, item.Quality);
            Assert.AreEqual(9, item.SellIn);
        }

        [Test]
        public void ExpirationDatePassed_DecreaseTwo()
        {
            var item = new Item { Name = "foo", SellIn = 0, Quality = 10 };
            _items.Add(item);
            _gildedRose.UpdateQuality();
            Assert.AreEqual("foo", item.Name);
            Assert.AreEqual(8, item.Quality);
            Assert.AreEqual(-1, item.SellIn);
        }

        [Test]
        public void ExpiredAgeBrie_QualityIncrease()
        {
            var item = new Item { Name = "Aged Brie", SellIn = 0, Quality = 10 };
            _items.Add(item);
            _gildedRose.UpdateQuality();
            Assert.AreEqual(12, item.Quality);
            Assert.AreEqual(-1, item.SellIn);
        }

        [TestCase(11, 11)]
        [TestCase(10, 12)]
        [TestCase(9, 12)]
        [TestCase(5, 13)]
        [TestCase(0, 0)]
        public void ConcertAge_QualityIncrease(int sellIn, int newQuality)
        {
            var item = new Item { Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = sellIn, Quality = 10 };
            _items.Add(item);
            _gildedRose.UpdateQuality();
            Assert.AreEqual(newQuality, item.Quality);
            Assert.AreEqual(sellIn - 1, item.SellIn);
        }

        [Test]
        public void ExpiredAgeBrie_QualityMaxIs50()
        {
            var item = new Item { Name = "Aged Brie", SellIn = 0, Quality = 50 };
            _items.Add(item);
            _gildedRose.UpdateQuality();
            Assert.AreEqual(50, item.Quality);
            Assert.AreEqual(-1, item.SellIn);
        }

        [Test]
        public void ExpiredAgeBrie_SulfurasDoesNothing()
        {
            var item = new Item { Name = "Sulfuras, Hand of Ragnaros", SellIn = 10, Quality = 10 };
            _items.Add(item);
            _gildedRose.UpdateQuality();
            Assert.AreEqual(10, item.Quality);
            Assert.AreEqual(10, item.SellIn);
        }

        [Test]
        public void ConjuredItem_DegradeTwoTimeFaster()
        {
            var item = new Item { Name = "Conjured Mana Cake", SellIn = 10, Quality = 10 };
            _items.Add(item);
            _gildedRose.UpdateQuality();
            Assert.AreEqual(8, item.Quality);
            Assert.AreEqual(9, item.SellIn);
        }

        [Test]
        public void QualityCantBeNegative()
        {
            var item = new Item { Name = "foo", SellIn = 0, Quality = 0 };
            _items.Add(item);
            _gildedRose.UpdateQuality();
            Assert.AreEqual("foo", item.Name);
            Assert.AreEqual(0, item.Quality);
        }
    }
}

